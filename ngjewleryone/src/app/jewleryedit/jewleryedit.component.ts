import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {JewleryService} from "../jewleryview/jewlery.service";

@Component({
  selector: 'app-jewleryedit',
  templateUrl: './jewleryedit.component.html',
  styleUrls: ['./jewleryedit.component.css']
})
export class JewleryeditComponent /*implements OnInit*/ {
  editCoin: any;
  angForm: FormGroup;
  title = 'Edit Coin';

  constructor(private route: ActivatedRoute, private router: Router, private service: JewleryService, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      catname: ['', Validators.required],
      catdesc: ['', Validators.required]
    });
  }

/*  updateCoin(catname, catdesc, id) {
    this.route.params.subscribe(params => {
      this.service.updateCoin(catname, catdesc, params['id']);
      this.router.navigate(['JewleryshowComponent']);
    });
  }*/
  /*ngOnInit() {
    this.route.params.subscribe(params => {
  /!*    this.editCoin = this.service.editCoin(params['id']).subscribe(res => {*!/
        this.editCoin = res;
      });
    });
  }*/
  deleteCoin(id) {
    this.service.deleteCoin(id).subscribe(res => {
      console.log('Deleted');
    });
  }
}


