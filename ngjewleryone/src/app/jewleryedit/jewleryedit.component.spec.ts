import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JewleryeditComponent } from './jewleryedit.component';

describe('JewleryeditComponent', () => {
  let component: JewleryeditComponent;
  let fixture: ComponentFixture<JewleryeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JewleryeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JewleryeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
