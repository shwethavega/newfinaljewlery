import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { JewleryviewComponent } from './jewleryview/jewleryview.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ConfigService} from './config.service';
import { JewleryshowComponent } from './jewleryshow/jewleryshow.component';
import { JewleryeditComponent } from './jewleryedit/jewleryedit.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    JewleryviewComponent,
    JewleryshowComponent,
    JewleryeditComponent
  ],
  imports: [
    BrowserModule, FormsModule,
    HttpClientModule, ReactiveFormsModule, RouterModule.forRoot([])

  ],
  providers: [
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
