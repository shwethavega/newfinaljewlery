import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {JewleryService} from "../jewleryview/jewlery.service";

@Component({
  selector: 'app-jewleryshow',
  templateUrl: './jewleryshow.component.html',
  styleUrls: ['./jewleryshow.component.css']
})
export class JewleryshowComponent implements OnInit {
  getCoin: any;

  constructor(private http: HttpClient, private service: JewleryService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getData().subscribe(res => {
      this.getCoin = res;
    });
  }
}


