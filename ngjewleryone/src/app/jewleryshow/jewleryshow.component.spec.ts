import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JewleryshowComponent } from './jewleryshow.component';

describe('JewleryshowComponent', () => {
  let component: JewleryshowComponent;
  let fixture: ComponentFixture<JewleryshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JewleryshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JewleryshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
