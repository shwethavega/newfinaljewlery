import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JewleryviewComponent } from './jewleryview.component';

describe('JewleryviewComponent', () => {
  let component: JewleryviewComponent;
  let fixture: ComponentFixture<JewleryviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JewleryviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JewleryviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
