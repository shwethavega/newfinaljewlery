import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {JewleryService} from "./jewlery.service";

@Component({
  selector: 'app-jewleryview',
  templateUrl: './jewleryview.component.html',
  styleUrls: ['./jewleryview.component.css']
})
export class JewleryviewComponent implements OnInit {

  title = 'Add Coin';
  angForm: FormGroup;
  constructor(private JewleryService: JewleryService, private fb: FormBuilder) {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      catname: ['', Validators.required ],
      catdesc: ['', Validators.required ]
    });
  }
  addCoin(catname, catdesc) {
    this.JewleryService.addCoin(catname, catdesc);
  }
  ngOnInit() {
  }
}
