import { Injectable } from '@angular/core';
import {HttpModule} from '@angular/http';

import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {ConfigService} from "../config.service";



@Injectable({
  providedIn: 'root'
})
export class JewleryService {
  result: any;

  constructor(private http: HttpClient, public ConfigService: ConfigService) {
  }

  addCoin(catname, catdesc) {
    const uri = this.ConfigService.baseURL + '/categories/create';
    const obj = {
      catname: catname,
      catdesc: catdesc
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res =>
        console.log('Done'));
  }

  getData() {
    const uri = this.ConfigService.baseURL + '/categories/All';
    return this
      .http
      .get(uri)
      .map(res => {
        return res;
      });
  }

 /* editCoin(id) {
    const uri =  'http://localhost:1234/categories/' + id + '/update';
    return this
      .http
      .get(uri)
      .map(res => {
        return res;
      });
  }

  updateCoin(catname, catdesc, id) {
    const uri = 'http://localhost:1234/categories/' + id + '/update';

    const obj = {
      catname: catname,
      catdesc: catdesc
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res => console.log('Done'));
  }
*/
  deleteCoin(id) {
    const uri = 'http://localhost:1234/categories/delete' + id ;


    return this
      .http
      .get(uri)
      .map(res => {
        return res;
      });
  }
}






